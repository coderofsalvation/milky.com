# MILKY.COM

![](https://github.com/coderofsalvation/MilkyTrackerX/raw/master/res/milkytrackerX.png)

# Usage

```
$ milky.com
usage   : milky.com [-o out.wav] [-t durationsec] [-s durationsmp] [song.xm in1.wav in2.wav ..] [-e <synthesizer expression>]
examples: milky.com -o synth.wav -t 1 -e 'lpf(env(sin(40), 0.0001, 0.1), 40*3)'
          milky.com -o song.wav  -t 1 song.xm -e 'hpf(input(0), 60)'
```

> This is the [cosmopolitan](https://github.com/jart/cosmopolitan)-cli counterpart of [MilkytrackerX](https://github.com/coderofsalvation/MilkyTrackerX)<br>
It runs natively on many architectures ♥<br>
Therefore milkytracker-creativity will live on forever (sortof) ♥ 

# Goals

* render `.XM`-songs (to `.wav`)
* render `.msyn`-textfile synthesizers (to `.wav)
* [future] batchprocess multiple xm files with `.msyn`-textfile for megamix/mastering purposes (album e.g.)
* [future] realtime output stream to aplay/paplay/ffmpeg (rtsp e.g.)
* [future] **milky.com albums**: allow enduser to zip/unzip `.XM`- or `.msyn`-files into `milky.com` 

# Why

Author has made digital music since his teens.<br>
Author has preserved lots of projectfiles since his teens.<br>
Most (old) musicsoftware does not run (old) projectfiles on modern computers anymore.<br>
Therefore, lots of data is trapped in music projectfiles.<br>
Author bit sad everytime when email arrives with remix request (for old tune).

> Musicsoftware with a [cosmopolitan](https://github.com/jart/cosmopolitan)-counterpart seems like a sane direction.


