#include <assert.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h> 
#include "milkysynth/milkysynth.c"
#include "wavread.h"
#include "wavwrite.h"

#define INPUTS 16
#define INPUTBUF 64
#define MAXSHORT 32767.0

#define print(fmstr,...) do {                    \
          fprintf(stderr,fmstr, ##__VA_ARGS__);  \
        } while(0)

#define error(fmstr,...) do {                    \
          fprintf(stderr,fmstr, ##__VA_ARGS__);  \
          exit(1);                               \
        } while(0)

typedef struct {
  float  *buf;
  float peak;
  int samples;
  int channels;
  int samplerate;
} Input;
  
int     inputs   = 0;
Input   in[INPUTS];
PcmFile *wav[INPUTS];  // dont include PcmFiles in Input-struct (allow non-pcmfile inputs)

short   *outbuf[INPUTS];
char    *outfile = NULL;
WavFile *out     = NULL;
int srate        = 44100;
int     outchan  = 44100;
int     outsamples  = 1000;
int     c        = 0; // current channel
  
void usage(){
	printf("usage: ./cli [-o out.wav] [-t durationsec] [-s durationsmp] [in1.wav in2.wav ..] -e 'lpf(env(sin(40), 0.0001, 0.1), 40*3)' [-n 64]\n");
	exit(0);
}

float sample_loader(const char *name, int slot, int frame){
  PcmFile *w = wav[slot];
  if( w == NULL ) return 0.0f;

  int chan = w->channels;
  if( slot > INPUTS-1 || frame >= w->samples || c >= chan ) return 0.0f;
  float f = in[slot].buf[ frame+c ];
  return f / MAXSHORT;
}

void load_wav(const char *file, int input){
  PcmFile *w = openWavRead(file,0); 
  wav[input] = w;
  if( w->samplerate != srate ) error("[e] %s has samplerate %i (instead of %i [-r %i])",file,w->samplerate,srate,srate);
  if( w != NULL ){
    int len = w->samples * sizeof(float) * w->channels;
    float *buf = malloc( len );
    wavReadFloat32(w, buf, len, NULL);
    in[input].buf = buf;
    in[input].samples    = w->samples;
    in[input].channels   = w->samples;
    in[input].samplerate = w->samplerate;
    in[input].peak     = 0.0;
    for( int i = 0; i < w->samples; i++ )
      if( fabs( in[input].buf[i]) > in[input].peak ) 
        in[input].peak = fabs( in[input].buf[i] );
  }
  inputs = 0; // recalc inputs
  for( int i = 0; i < INPUTS; i++ ) 
    if( wav[i] != NULL ) inputs++;
}

void calcOutput(){
  if( outsamples != -1 ) return;
  printf("calc\n");
  for( int i = 0; i < INPUTS; i++ ){
    if( wav[i] == NULL ) continue;
    if( wav[i]->samples  > outsamples  ) outsamples = wav[i]->samples;
    if( wav[i]->channels > outchan     ) outchan    = wav[i]->channels; 
  }
}

void cleanup(){
	for( int i = 0; i < INPUTS; i++ ){
    if( wav[i] != NULL ){ 
      closeWavRead(wav[i]);
      free(in[i].buf);
    }
    if( outbuf[i] != NULL ) free(outbuf[i]);
  }
	if( outfile != NULL ){ 
    closeWavWrite(out);
    printf("written %s\n",outfile);
  }
}

int main(int argc, char *argv[]){
	int opt;
  int ok        = 0;
  int note      = 64;
	char *expr    = NULL;
  for( int i =0; i < INPUTS; i++, wav[i] = NULL, in[i].buf = NULL ){}

	while((opt = getopt(argc, argv, ":s:e:n:o:t:")) != -1) 
	{ 
			switch(opt) 
			{ 
					case 'r':  { srate   = atoi(optarg); break; };
					case 'e':  { expr    = optarg;       break; }; 
					case 'n':  { note    = atoi(optarg); break; };
					case 'o':  { outfile = optarg;       break; }; 
					case 't':  { outsamples  = atoi(optarg)*srate; break; }; 
					case 's':  { outsamples  = atoi(optarg);       break; }; 
					//case '?': usage(); break;
			} 
	} 
	for(int x = 0; optind < argc; optind++){ // load input file(s)
    load_wav(argv[optind],x);
    x++;
  }

	if( argc < 2 ) usage();
  calcOutput();
  if( outfile != NULL ){ 
    out = openWavWrite( outfile,srate,outchan,WAV_FMT_16BIT,OUTPUT_WAV,0);
    for( int i = 0; i < outchan; i++ ) outbuf[i] = malloc( outsamples * sizeof(short) ); 
  }

  // generate + write
  struct milkysynth *g = milkysynth_create();
  milkysynth_set_sample_loader( &sample_loader, NULL );
  libmilkysynth_init(srate,0);
  for( c = 0; c < outchan; c++ ){
    if( milkysynth_compile(g, expr, strlen(expr)) == 0 ){
      int i = 0;
      for ( i = 0; i < outsamples; i++) {
        float f = milkysynth_eval(g);
        if( outfile == NULL ) fwrite( (const void *__restrict )&f,1, sizeof(f),stdout);
        else outbuf[c][ i ] = (short) (f * MAXSHORT);
      }
      printf("outsamples = %i\n",i);
    }else {
      print("error compiling script\n");
      exit(1);
    }
  }
  writeWavData(out, outbuf[0],outsamples,0);
  milkysynth_destroy(g);

  cleanup();

}
