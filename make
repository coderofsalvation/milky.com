#!/bin/sh 
VERSION=0.9
APP=milky
BIN=./$APP.com
OBJECTS="wavread.c wavwrite.c milky.c"
test -z $COSMO_PATH && export COSMO_PATH=~/projects/cosmopolitan

native(){
  gcc -o $APP ${OBJECTS} -lm
}

cosmo(){
  test -z $COSMO_PATH && { echo "[e] COSMO_PATH env-var not set (should point to https://github.com/jart/cosmopolitan)"; exit 1; }
  test -d cosmo || {
    mkdir cosmo && cd cosmo
    wget https://justine.lol/cosmopolitan/cosmopolitan.zip
    unzip -o cosmopolitan.zip 
  }
  test -f $APP.com && rm $APP.com
  set -x
  gcc -DVERSION=\"$VERSION\" -DCOSMO -g3 -Og -static -nostdlib -nostdinc -fno-pie -no-pie -mno-red-zone -pg \
      -fno-omit-frame-pointer -pg -mnop-mcount -I$COSMO_PATH/libc -I$COSMO_PATH/libc/stdio -I$COSMO_PATH/libc/isystem -I$COSMO_PATH -I./fake \
      -o $APP.com.dbg ${OBJECTS} -fuse-ld=bfd -Wl,-T,cosmo/ape.lds \
      -include cosmo/cosmopolitan.h cosmo/crt.o cosmo/ape.o cosmo/cosmopolitan.a
  objcopy -S -O binary $APP.com.dbg $APP.com && ls -lah $APP.com
}

tests(){
  #$BIN_test 
  cp /home/leon/Downloads/loop.wav.slice001.wav /tmp/in.wav
  set -x
  test -f /tmp/out.wav && rm /tmp/out.wav
  $BIN
  #$BIN -o /tmp/out.wav -e 'mix( 0.8*hpf(input(0)+input(1),300,1) )' /tmp/in*.wav #| ${player}
  $BIN -o /tmp/out.wav -e 'fm( 1300, 0.5, 1) * 0.3' #| ${player}
  #$BIN -t 1 -o /tmp/out.wav -e 'hpf( byte((t*t/256)&(t>>((t/1024)%16))^t%64*(828188282217>>(t>>9&30)&t%32)*t>>18) , 200, 1)' #| ${player}
  #$BIN -o /tmp/out.wav /tmp/in*.wav -e 'mix( lpf(input(0), 120, 1) )' | ${player}
  #$BIN -o /tmp/out.wav /tmp/in*.wav -e 'vocode( input(0), input(1), 16, 0.5, 0.5, 0.4, 0.4 )' | ${player}
  play /tmp/out.wav
  #$BIN -1 /tmp/in.wav -e 'mix( lpf(env(sin(40), 0.0001, 0.1), 40*3) )' | ${player}
  #$BIN -o /tmp/out.wav -1/tmp/in.wav -e"$(cat ../examples/drums.glitch)"
  #find ../examples -type f | while read example; do 
  #  $BIN -e "$(cat $example)" - | ${player}
  #done
}

test -f milkysynth.com && rm milkysynth.com*
test -z $1 && cosmo 2>&1 | less
test -z $1 || "$@"

